// Copyright 2015 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// This package is an example project that shows an example use for the command
// fileembed-go.
//
// The program is a small HTTP server that serves one of two  HTML files.  These
// HTML files are converted using fileembed-go into a single source file called
// files.go.  When the package is built, the byte array variables declared in
// files.go have the same contents as the original files, and can be served
// directly.
//
// If this program was deployed, only the binary would be required.  The HTML
// source files are not required.
//
// Look at the Makefile for an example use.
// If you are using 'go generate', the following comment is
// used to create or update files.go.
//
//     //go:generate fileembed-go -filename files.go index.html sub.html
package main

import (
	"log"
	"net/http"
)

// The program won't work without first creating files.go.  This file can be
// created or updated by using 'go generate'.
//
//go:generate fileembed-go -filename files.go index.html sub.html

// The web server...
func Server(w http.ResponseWriter, req *http.Request) {
	path := req.URL.Path
	log.Printf("Access %s %s\n", req.Method, path)
	if req.Method != "GET" {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}

	if path[0] == '/' {
		path = path[1:]
	}
	data, ok := embeddedfiles[path]
	if !ok {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	_, err := w.Write(data)
	if err != nil {
		log.Printf("Error writing response: %v", err)
	}
}

// Main.  See https://golang.org/ref/spec#Program_execution if this is
// unclear...
func main() {
	http.HandleFunc("/", Server)
	err := http.ListenAndServe(":12345", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err.Error())
	}
}
