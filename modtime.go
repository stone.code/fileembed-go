// Copyright 2015 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"os"
	"time"
)

func write_modtime(out *os.File) error {
	if !*modtime {
		return nil
	}

	modtime := time.Now().UTC()

	out.WriteString("const ModTime = \"")
	out.WriteString(modtime.String())
	_, err := out.WriteString("\"\n\n")
	return err
}
