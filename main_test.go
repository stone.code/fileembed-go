// Copyright 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"bytes"
	"fmt"
	"go/parser"
	"go/token"
	"io/ioutil"
	"math/rand"
	"os"
	"os/exec"
	"path/filepath"
	"testing"
	"testing/quick"
)

var (
	testBlobs = []string{"abcd", "abcd\nabcd", "abcd\x00", "\x01\x02\x03\x7f\x80\x81\xfe\xff",
		"abcdefghijklmnopqrstuvxyz0123456789",
	}
)

func TestWriteLine0(t *testing.T) {
	buffer := bytes.NewBuffer(nil)
	err := write_line(buffer, nil)
	if err != nil {
		t.Errorf("Unexpected error, %s", err)
	}
	if buffer.String() != "" {
		t.Errorf("Incorrect output from write_line, %s", buffer.String())
	}
}

func TestWriteLine10(t *testing.T) {
	buffer := bytes.NewBuffer(nil)
	err := write_line(buffer, []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10})
	if err != nil {
		t.Errorf("Unexpected error, %s", err)
	}
	if buffer.String() != "0,1,2,3,4,5,6,7,8,9,10,\n" {
		t.Errorf("Incorrect output from write_line, %s", buffer.String())
	}
}

func TestWriteLine(t *testing.T) {
	f := func(x []byte) bool {
		buffer := bytes.NewBuffer(nil)
		err := write_line(buffer, x)
		return err == nil
	}
	if err := quick.Check(f, nil); err != nil {
		t.Error(err)
	}
}

func TestVerifyFlags(t *testing.T) {
	test_cases := []struct {
		filename string
		format   string
		expected bool
	}{
		{"", "", false},
		{"t.txt", "", false},
		{"t.txt", "[]byte", true},
		{"", "[]byte", false},
		{"t.txt", "string", true},
		{"t.txt", "---", false},
	}

	for _, v := range test_cases {
		*filename = v.filename
		*format = v.format
		ok, _ := verify_flags()
		if v.expected != ok {
			t.Errorf("Mismatch on test %v", v)
		}
	}
}

func TestDefaults(t *testing.T) {
	const prefix = `package main

var file0 = [...]byte{
`
	const suffix = `
}

var embeddedfiles = map [string] []byte {
	"%s": file0[0:],
}
`
	// These should match the test blobs
	expected := []string{
		"97,98,99,100,",
		"97,98,99,100,10,97,98,99,100,",
		"97,98,99,100,0,",
		"1,2,3,127,128,129,254,255,",
		"97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,120,121,122,48,49,50,51,52,53,54,\n55,56,57,",
	}
	for i := range testBlobs {
		expected[i] = prefix + expected[i] + suffix
	}

	*format = "[]byte"
	*verbose = false
	testPairs(t, testBlobs, expected)
}

func TestFormatString(t *testing.T) {
	const prefix = `package main

var file0 = "`
	const suffix = `"

var embeddedfiles = map [string] string {
	"%s": file0,
}
`
	expected := []string{
		"\\x61\\x62\\x63\\x64",
		"\\x61\\x62\\x63\\x64\\x0a\\x61\\x62\\x63\\x64",
		"\\x61\\x62\\x63\\x64\\x00",
		"\\x01\\x02\\x03\\x7f\\x80\\x81\\xfe\\xff",
		"\\x61\\x62\\x63\\x64\\x65\\x66\\x67\\x68\\x69\\x6a\\x6b\\x6c\\x6d\\x6e\\x6f\\x70\\x71\\x72\\x73\\x74\\x75\\x76\\x78\\x79\\x7a\\x30\\x31\\x32\\x33\\x34\\x35\\x36\\x37\\x38\\x39",
	}
	for i := range testBlobs {
		expected[i] = prefix + expected[i] + suffix
	}

	*format = "string"
	*verbose = false
	testPairs(t, testBlobs, expected)
}

func TestPackageName(t *testing.T) {
	const prefix = `package mypackage

var file0 = [...]byte{
`
	const suffix = `
}

var embeddedfiles = map [string] []byte {
	"%s": file0[0:],
}
`
	// These should match the test blobs
	expected := []string{
		"97,98,99,100,",
		"97,98,99,100,10,97,98,99,100,",
		"97,98,99,100,0,",
		"1,2,3,127,128,129,254,255,",
		"97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,120,121,122,48,49,50,51,52,53,54,\n55,56,57,",
	}
	for i := range testBlobs {
		expected[i] = prefix + expected[i] + suffix
	}

	*packagename = "mypackage"
	*format = "[]byte"
	*verbose = false
	testPairs(t, testBlobs, expected)
}

func testPairs(t *testing.T, testFiles []string, expected []string) {
	for i := range testFiles {
		file, err := ioutil.TempFile("", "test")
		if err != nil {
			t.Fatalf("Could not create temporary file, %s", err)
		}
		defer closeTempFile(t, file)
		_, err = file.WriteString(testFiles[i])
		if err != nil {
			t.Fatalf("Could not create temporary file, %s", err)
		}

		*filename = "./go-test.tmp"
		err = main2([]string{file.Name()})
		if err != nil {
			t.Errorf("Could not process file, %s", err)
		}
		output, err := readFile(*filename)
		if err != nil {
			t.Errorf("Could not read output file, %s", err)
		}

		expected := fmt.Sprintf(expected[i], filepath.ToSlash(file.Name()))
		if output != expected {
			writeFile("./go-test-expected.tmp", expected)
			t.Fatalf("Mismatch in output file %d\n--\n%s\n--\n%s", i, expected, output)
		}

		// For the truly paranoid, we make sure that we can parse the generated file.
		// TODO:  Can we extract the literal byte array or string, and verify the
		// contents against the test blobs?
		_, err = parser.ParseFile(token.NewFileSet(), *filename, output, parser.AllErrors)
		if err != nil {
			t.Fatalf("Could not parse the source file, %s", err)
		}

		// This block cannot be put into a defer, as we need to clean this file
		// up at the end of every loop.
		err = os.Remove(*filename)
		if err != nil {
			t.Logf("Could not remove temporary file, %s", err)
		}
	}
}

func closeTempFile(t *testing.T, file *os.File) {
	name := file.Name()
	err := file.Close()
	if err != nil {
		t.Logf("Could not close file, %s", err)
	}
	err = os.Remove(name)
	if err != nil {
		t.Logf("Could not remove file, %s", err)
	}
}

func readFile(filename string) (string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}

	return string(bytes), nil
}

func writeFile(filename string, data string) error {
	file, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.WriteString(data)
	return err
}

func BenchmarkFormatBytes1KB(b *testing.B) {
	b.StopTimer()
	benchmarkFormat(b, 1024)
}

func BenchmarkFormatBytes10KB(b *testing.B) {
	b.StopTimer()
	benchmarkFormat(b, 10*1024)
}

func BenchmarkFormatBytes100KB(b *testing.B) {
	b.StopTimer()
	benchmarkFormat(b, 100*1024)
}

func BenchmarkFormatBytes1MB(b *testing.B) {
	b.StopTimer()
	benchmarkFormat(b, 1024*1024)
}

func BenchmarkFormatString1KB(b *testing.B) {
	b.StopTimer()
	benchmarkFormat(b, 1024)
}

func BenchmarkFormatString10KB(b *testing.B) {
	b.StopTimer()
	benchmarkFormat(b, 10*1024)
}

func BenchmarkFormatString100KB(b *testing.B) {
	b.StopTimer()
	benchmarkFormat(b, 100*1024)
}

func BenchmarkFormatString1MB(b *testing.B) {
	b.StopTimer()
	benchmarkFormat(b, 1024*1024)
}

func benchmarkFormat(b *testing.B, size int) {
	b.StopTimer()
	err := writeFile("./benchmark/go-bench-main.go",
		`package main

import "fmt"

func main() {
	fmt.Printf("%v", file0)
}
`)
	if err != nil {
		b.Fatalf("Could not write temporary file, %s", err)
	}

	// Create a dummy file
	buf := make([]byte, size)
	for i, v := range rand.Perm(size / 2) {
		buf[2*i] = byte(v & 0xFF)
		buf[2*i+1] = byte((v >> 8) & 0xFF)
	}
	err = writeFile("./benchmark/go-bench.bin", string(buf))
	if err != nil {
		b.Fatalf("Could not write temporary file, %s", err)
	}
	*filename = "./benchmark/go-bench.go"
	*format = "[]byte"
	*packagename = "main"
	*verbose = false
	err = main2([]string{"./benchmark/go-bench.bin"})
	if err != nil {
		b.Fatalf("Could not write temporary file, %s", err)
	}

	b.StartTimer()
	for i := 0; i < b.N; i++ {
		cmd := exec.Command("go", "build")
		cmd.Dir = "./benchmark"
		err = cmd.Run()
		if err != nil {
			b.Fatalf("Could not compile the temporary package, %s", err)
		}
	}
	b.StopTimer()

	fi, err := os.Stat("./benchmark/benchmark")
	if err != nil {
		b.Errorf("Did not find the compiled library.  Stats are probably incorrect.")
	} else {
		b.Logf("Binary size: %d", fi.Size())
	}
}
