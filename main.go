// Copyright 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

var (
	filename        = flag.String("filename", "", "Filename for the generated Go source")
	force_lowercase = flag.Bool("force-lowercase", false, "Force filenames to lowercase for the embedded map")
	format          = flag.String("format", "[]byte", "Embed the data as either byte arrays or strings")
	use_basename    = flag.Bool("basename", false, "Only include the files basename in the embedded map")
	modtime         = flag.Bool("modtime", false, "Add a variable with the time that the file was generated")
	packagename     = flag.String("package", "main", "Package name for the generated Go source")
	verbose         = flag.Bool("verbose", true, "Provide extra diagnostic messages during processing")
)

// Write out one line of the encoded file.
func write_line(out io.Writer, data []byte) error {
	if len(data) == 0 {
		return nil
	}
	for lp := 0; lp < len(data); lp++ {
		_, err := out.Write([]byte(strconv.Itoa(int(data[lp])) + ","))
		if err != nil {
			return err
		}
	}
	_, err := out.Write([]byte{'\n'})
	return err
}

// Encode a file as Go source for a byte array.
func write_byte_array_literal(out *os.File, in *os.File) error {
	_, err := out.WriteString("[...]byte{\n")
	if err != nil {
		return err
	}

	buffer := [32]byte{}
	n, err := in.Read(buffer[:])
	for err == nil {
		err = write_line(out, buffer[0:n])
		if err != nil {
			return err
		}
		n, err = in.Read(buffer[:])
	}
	if err != io.EOF {
		return err
	}

	_, err = out.WriteString("}")
	return err
}

// Encode a file as Go source for a string.
func write_string_literal(out *os.File, in *os.File) error {
	const hexdigits = "0123456789abcdef"

	_, err := out.WriteString("\"")
	if err != nil {
		return err
	}

	buffer := [32]byte{}
	n, err := in.Read(buffer[:])
	for err == nil {
		for _, v := range buffer[0:n] {
			_, err = out.Write([]byte{'\\', 'x', hexdigits[v>>4], hexdigits[v&0xF]})
			if err != nil {
				return err
			}
		}
		n, err = in.Read(buffer[:])
	}
	if err != io.EOF {
		return err
	}

	_, err = out.WriteString(`"`)
	return err
}

func main() {
	// Verify the command line arguments
	flag.Parse()
	ok, msg := verify_flags()
	if !ok {
		fmt.Fprintf(os.Stderr, "%s\n\n", msg)
		flag.Usage()
		os.Exit(1)
	}

	// Run the requested action.  This is done in a separate routine to provide
	// testability.
	err := main2(flag.Args())
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: ", err)
		os.Exit(1)
	}
}

func verify_flags() (bool, string) {
	if *filename == "" {
		return false, "Command-line argument 'filename' is required."
	}
	if *format != "[]byte" && *format != "string" {
		return false, "Command-line argument 'format' must be either '[]byte' or 'string'."
	}
	return true, ""
}

func main2(input_filenames []string) error {
	// open the output file
	file, err := os.OpenFile(*filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.WriteString("package " + *packagename + "\n\n")
	if err != nil {
		return err
	}
	err = write_modtime(file)
	if err != nil {
		return err
	}
	for lp, item := range input_filenames {
		if *verbose {
			fmt.Println("Processing:", item)
		}
		err = process_item(file, lp, item)
		if err != nil {
			return err
		}
	}

	file.WriteString("var embeddedfiles = map [string] " + *format + " {\n")
	for lp, item := range input_filenames {
		if *format == "string" {
			file.WriteString("\t\"" + embed_filename(item) + "\": file" + strconv.Itoa(lp) + ",\n")
		} else {
			file.WriteString("\t\"" + embed_filename(item) + "\": file" + strconv.Itoa(lp) + "[0:],\n")
		}
	}
	_, err = file.WriteString("}\n")
	if err != nil {
		return err
	}
	if *verbose {
		fmt.Println("Done.")
	}
	return nil
}

func process_item(out *os.File, ndx int, filename string) error {
	// Open the input file to start conversion
	in, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer in.Close()

	// Write the start of the variable declaration
	_, err = out.WriteString("var file" + strconv.Itoa(ndx) + " = ")
	if err != nil {
		return err
	}

	// Create the byte array literal or string literal
	switch *format {
	case "string":
		err = write_string_literal(out, in)
	case "[]byte":
		err = write_byte_array_literal(out, in)
	default:
		// The command-line inputs should have been verified earlier.
		panic("Unrecognized format specifier.")
	}
	if err != nil {
		return err
	}

	// Write out a new line to terminate the variable declaration
	_, err = out.WriteString("\n\n")
	return err
}

func embed_filename(filename string) string {
	if *use_basename {
		filename = filepath.Base(filename)
	}
	if *force_lowercase {
		filename = strings.ToLower(filename)
	}
	filename = filepath.ToSlash(filename)
	return filename
}
