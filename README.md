# Introduction

This is a command-line utility to take a number of source files, and embed
them into a Go package.
The utility converts the source files, typically static assets required
by the binary, and creates a new Go source file with a byte array (or a
string) for each source file.  The byte arrays are initialized with the
contents of the source files.

Additionally, the Go file created will contain a map, called 'embeddedfiles',
that can be used to lookup the binary data using the filename.  The keys of
this map can be modified with the command-line options -basename and
-force-lowercase.  The filenames will be converted to a platform independent
form by converting all path separators to forward slashes.

By setting -modetime=true, the new Go source file will contain a const value
called 'ModTime', which will contain a string representation of the time
that the source file was generated.  This can be used to support caching of
the embedded data, such as with http.ServeContent.

## Usage

     Usage of ./fileembed-go:
       -basename=false: Only include the files basename in the embedded map
       -filename="": Filename for the generated Go source
       -force-lowercase=false: Force filenames to lowercase for the embedded map
       -format="[]byte": Embed the data as either byte arrays or strings
       -modtime=false: Add a variable with the time that the file was generated
       -package="main": Package name for the generated Go source
       -verbose=true: Provide extra diagnostic messages during processing